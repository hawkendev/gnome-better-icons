Name:           gnome-better-icons
Version:        1.2.0
Release:        0%{?dist}
Summary:        Modern icon and cursor themes for the gnome desktop
License:        GPLv3+
URL:            https://www.gnome-look.org
BuildArch:      noarch

Requires:       gnome-shell
BuildRequires:  git
BuildRequires:  tar
BuildRequires:  wget

%description
Modern icon and cursor themes for the gnome desktop. Restart required.

%prep
git clone https://gitlab.com/hawkendev/gnome-better-icons.git
wget https://sourceforge.net/projects/gnome-better-icons/files/icon-themes.tar.xz # Might use curl in the future
wget https://sourceforge.net/projects/gnome-better-icons/files/cursor-themes.tar.xz # Might use curl in the future

%build
tar -xf cursor-themes.tar.xz
tar -xf icon-themes.tar.xz

%install
# Install icons
mkdir -p %{buildroot}/%{_datadir}/icons
cp -ra cursor-themes/. %{buildroot}/%{_datadir}/icons
cp -ra icon-themes/. %{buildroot}/%{_datadir}/icons
# Install icon setup script (once per user, run on startup)
mkdir -p %{buildroot}/etc/profile.d
cp -ra gnome-better-icons/package-content/set-initial-better-icons.sh %{buildroot}/etc/profile.d

%files
%{_datadir}/icons/*
/etc/*
