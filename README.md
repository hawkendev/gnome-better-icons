## Bundled Cursor themes
- https://github.com/yeyushengfan258/ArcStarry-Cursors
- https://github.com/yeyushengfan258/Polarnight-Cursors
- https://github.com/yeyushengfan258/We10XOS-cursors
- https://github.com/yeyushengfan258/WinSur-white-cursors
- https://github.com/ful1e5/XCursor-pro

## Bundled Icon Themes
- https://github.com/vinceliuice/Fluent-icon-theme
- https://github.com/TaylanTatli/Sevi
- https://github.com/vinceliuice/McMojave-circle
- https://github.com/yeyushengfan258/Win11-icon-theme


Theme files are hosted on [SourceForge](https://sourceforge.net/projects/gnome-better-icons/)
